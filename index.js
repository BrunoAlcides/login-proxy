const path = require('path')
const fs = require('fs')
const express = require('express')
const httpProxy = require('http-proxy-middleware')
const dynamicMiddleware = require('express-dynamic-middleware')
const bodyParser = require('body-parser')
const util = require('util')
const cors = require('cors')
const trumpet = require('trumpet')
const through = require('through')

const weboutgoing = require('./http-proxy-middleware/node-http-proxy/lib/http-proxy/passes/web-outgoing')

const wo = Object.values(weboutgoing)

const fsReaddir = util.promisify(fs.readdir);

const middlewares = dynamicMiddleware.create()
const app = express()

app.use(bodyParser.json())
app.use(cors())

const PORT = process.env.PORT || 1000
const MINUTES = 60000
const SISCOMEX_ORIGIN_URL = 'www1c.siscomex.receita.fazenda.gov.br'
const STATIC_FILES_EXTENSIONS = ['js', 'gif', 'css', 'jpg', 'jpeg', 'png']
const FILES_FILTER = STATIC_FILES_EXTENSIONS.map(file => `*.${file}`)
const TIME_TO_INVALIDATE_PROXY = MINUTES * 20
const SERVER_BASE_URL = 'localhost:1000'
const IMAGES = ['jpeg', 'jpg', 'gif', 'png', 'ico']

const generateFakeUUID = () => ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, a => (a ^ Math.random() * 16 >> a / 4).toString(16))

function joinWithRootPath(file) {
  return path.join(__dirname, file)
}

const directStaticFilesProxy = httpProxy({
  target: {
    protocol: 'https:',
    host: SISCOMEX_ORIGIN_URL,
    port: 443,
    pfx: fs.readFileSync(joinWithRootPath('./certs/rubens2020.pfx')),
    passphrase: 'rubens2020',
  },
  secure: false,
  changeOrigin: true
})

function removeMiddlewareProxy(middlewareFunction) {
  middlewares.unuse(middlewareFunction)
}

const siscomexProxy = (id, certName, password) => {
  const certPassword = password

  let sessionTimeout

  const optionsTarget = {
    protocol: 'https:',
    host: SISCOMEX_ORIGIN_URL,
    port: 443,
    pfx: fs.readFileSync(joinWithRootPath(`./certs/${certName}.pfx`)),
    passphrase: certPassword
  }

  const proxyOptions = {
    target: optionsTarget,
    secure: false,
    changeOrigin: true,
    forceRewrite: true,
    selfHandleResponse: true,
    protocolRewrite: 'http',
    hostRewrite: `${SERVER_BASE_URL}/proxy/${id}`,
    pathRewrite: (path) => {
      clearTimeout(sessionTimeout)
      sessionTimeout = setTimeout(invalidateProxy, TIME_TO_INVALIDATE_PROXY)

      return path.replace(`/proxy/${id}`, '')
    },
  }

  const proxy = httpProxy(`/proxy/${id}`, {
    target: optionsTarget,
    secure: false,
    changeOrigin: true,
    forceRewrite: true,
    selfHandleResponse: true,
    protocolRewrite: 'http',
    hostRewrite: `${SERVER_BASE_URL}/proxy/${id}`,
    pathRewrite: (path) => {
      clearTimeout(sessionTimeout)
      sessionTimeout = setTimeout(invalidateProxy, TIME_TO_INVALIDATE_PROXY)

      return path.replace(`/proxy/${id}`, '')
    },
    onProxyRes(proxyRes, req, res) {
      for (var i = 0; i < wo.length; i++) {
        if (wo[i](req, res, proxyRes, proxyOptions)) {
          break;
        }
      }

      const tr = trumpet()

      tr.pipe(res)

      tr.selectAll('img[src*="www1.siscomex.receita.fazenda.gov.br:443"],form[action]', elem => {
        const stream = elem.createStream({
          outer: true
        });

        stream.pipe(through(function (buf) {
          const isImg = Boolean(elem.getAttribute('src'))

          const content = buf.toString()

          const parsedContent = isImg
            ? content
              .replace(new RegExp('https://www1.siscomex.receita.fazenda.gov.br:443', 'g'), `http://${SERVER_BASE_URL}`)
              .replace('./i', `http://${SERVER_BASE_URL}/i`)
            : content.replace('action="', `action="${id}`)

          this.queue(parsedContent)
        })).pipe(stream)
      })

      proxyRes.pipe(tr)
    }
  })

  const invalidateProxy = removeMiddlewareProxy.bind(null, proxy)

  sessionTimeout = setTimeout(invalidateProxy, TIME_TO_INVALIDATE_PROXY)

  return proxy
}

const createProxy = async (req, res) => {
  const id = generateFakeUUID()
  const {
    certName,
    password
  } = req.body

  const dir = await fsReaddir(joinWithRootPath('./certs'))

  const hasCertfile = dir.some(file => file === `${certName}.pfx`)

  if (!hasCertfile) {
    return res
      .status(400)
      .end('Invalid certName.')
  }

  middlewares.use(siscomexProxy(id, certName, password))

  res.send(`http://${SERVER_BASE_URL}/proxy/${id}/siscomexImpweb-7/private_siscomeximpweb_inicio.do`)
}

app.post('/create-proxy', createProxy)

app.use(middlewares.handle())
app.use([...FILES_FILTER, '!/proxy/**'], directStaticFilesProxy)

app.listen(PORT, function () {
  console.log('App listening on port 1000!')
})
