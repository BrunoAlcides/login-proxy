FROM node:12-alpine

COPY . /app
WORKDIR /app

RUN yarn

EXPOSE 1000

CMD yarn start